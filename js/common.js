// 引导页弹窗
function allHeight() {
  var h = $("body").children("div");
  n = h.length - 2
  for (var i = 0; i < n; i++) {
    var ah = $(this).outerHeight() * n;
    $("#popBg").css("height", ah);
  }
}

function popUp() {
  $("#popBg,#popNav").show();
  $("body").css("overflow", "hidden");
  $(window).scroll(function () {
    $(this).scrollTop(0)
  });
  $(document).bind("touchmove", function (e) {
    e.preventDefault();
  });
  allHeight();
}

function popDown() {
  $("#popBg,#popNav").hide();
  $("body").css("overflow", "");
  $(window).unbind("scroll");
  $(document).unbind("touchmove");
}

// 分析页自适应
function imgHeight() {
  var bodyWidth = document.body.offsetWidth;
  $("#analyseImg").css({
    "height": bodyWidth / 1.1774 + "px",
    "line-height": bodyWidth / 1.3 + "px"
  });
  $("body").css("background", "url('img/bg_03.png')");
}

// 图片题
function imgChoice() {
  var ddWidth = $(".answer_nav_01 dl dd").width();
  $(".choice").css({
    "width": ddWidth - 4 + "px",
    "height": ddWidth - 4 + "px"
  });
  $(".answer_nav_01 dl dd img").css({
    "width": ddWidth - 4 + "px",
    "height": ddWidth - 4 + "px"
  });
  $(".answer_nav_01 dl dd span").css({
    "margin-top": ddWidth + "px"
  });
  $(".answer_nav_01 dl dd img").click(function () {
    $(".answer_nav_01 dl dd").removeClass("current");
    $(".answer_nav_02 a").addClass("current");
    $(this).parent().addClass("current");
  })
}

// 文字题
function textChoice() {
  $(".answer_nav_01 dl dd").click(function () {
    $(".answer_nav_01 dl dd").removeClass("current");
    $(".answer_nav_02 a").addClass("current");
    $(this).addClass("current");
  })
}